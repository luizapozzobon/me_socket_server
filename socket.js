const express = require('express');
const WebSocket = require('ws');
const fs = require('fs');
const https = require('https');

const app = express()
  // .use((req, res) => console.log("it's on") )

//var privateKey  = fs.readFileSync('sslcert/key.pem', 'utf8');
//var certificate = fs.readFileSync('sslcert/cert.pem', 'utf8');
//var credentials = {key: privateKey, cert: certificate};
//var httpsServer = https.createServer(credentials, app);
//httpsServer.listen(process.env.PORT || 15315, () => console.log(`Listening on ${ process.env.PORT || 15315 }`));

let server = app.listen(process.env.PORT || 15315, () => console.log(`Listening on ${ process.env.PORT || 15315 }`));

const wss = new WebSocket.Server({ server: server });

wss.binaryType = "arraybuffer";

let robot_id = 0,
user_id = 1,
lookup = {},
robots_ids = {} // para enviar as mensagens
robots_list = [], // para display para os usuários do site
users = {},
avatar_id = null;

let last_message = "";

// todas as infos dos robos conectados
robots = new Object;
robots.robot = new Array;

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    message = JSON.parse(message)
    console.log(message)
    let data = {
      data: message.data,
      origin: message.origin,
      action: message.action,
      destiny: message.destiny
    }
    console.log(data)
    if(data == last_message){
      console.log("Igual a ultima mensagem. Pulando.")
      data = "";
    }
    // ------------------ connection and disconnetion to socket server -----------

    if (data.action == 'register_robot') {
      robots.robot.push({default_user:'', default_origin: '', is_connected: false, name: data.origin, id: robot_id})
      robots.robot = robots.robot.filter((robot, index, self) => self.findIndex((r) => {return robot.name === r.name; }) === index)

      robots_ids[data.origin] = robot_id;
      robots_list[robot_id] = data.origin;
      ws.robot_id = robot_id;
      lookup[ws.robot_id] = ws;
      robot_id = robot_id + 2;

      // para eliminar repeticao de conexao de mesmo robo
      var uniq = robots_list
      .map((robot) => {
        return {count: 1, robot: robot}
      })
      .reduce((a, b) => {
        a[b.robot] = (a[b.robot] || 0) + b.count
        return a
      }, {})

      robots_list = Object.keys(uniq).sort((a, b) => uniq[a] < uniq[b])
      console.log(robots_list, robots_ids)

      if (robots_ids >= 1000) {
        robots_ids = 0;
      }
    }

    if (data.action == 'register_user') {
      users[data.origin] = user_id;
      ws.user_id = user_id;
      lookup[ws.user_id] = ws;
      user_id = user_id + 2;
      console.log(users);

      if (user_id > 1000) {
        user_id = 1;
      }
    }

    if (data.action == 'register_avatar') {
      avatar_id = 10101010;
      ws.avatar_id = avatar_id;
      lookup[ws.avatar_id] = ws;
    }

    if(data.action === 'connect_to_robot') {

      if (data.destiny) {
        var i = robots.robot.findIndex((r) => {return r.name === data.destiny});

        // admin = true; user = false
        if((data.data === true && robots.robot[i].default_user === true) ||
        (data.data === false && robots.robot[i].default_user === false) ||
        (data.data === false && robots.robot[i].default_user === true)) {
          // Admin1 x Admin2, admin1 keeps connection
          // Any x user, any keeps connection
          console.log(data.data, robots.robot[i].default_user)
          lookup[users[data.origin]].send(JSON.stringify({data: robots.robot[i].default_user, action: 'not_connected'}))
        } else if ((data.data === true && robots.robot[i].default_user === false)) {
          // Admin overwrites user privileges to connection
            // User x Admin = Admin gets connection
          robots.robot[i].is_connected = false;
          lookup[users[robots.robot[i].default_origin]].send(JSON.stringify({data: data.data, action: 'disconnected'}));
        }

        if(robots.robot[i].is_connected === false) {
          robots.robot[i].default_user = data.data;
          robots.robot[i].default_origin = data.origin;
          robots.robot[i].is_connected = true;
        }
      }
    }

    if (data.action === 'close_connection') {
      if (data.data === 'robot') {
        var index = robots.robot.findIndex((w) => {return w.name === data.origin});
        if (index > -1) {
          robots.robot.splice(index, 1);
        }

        index = robots_list.indexOf(data.origin)
        if (index > -1) {
          robots_list.splice(index, 1);
        }
        delete robots_ids[data.origin];

        wss.broadcast(JSON.stringify({action: 'robot_disconnected', origin: data.origin}));
      }

      if(data.data === 'website') {
        var i = robots.robot.findIndex((w) => {return w.default_origin === data.origin});
        if(i > -1) {
          robots.robot[i].default_origin = '';
          robots.robot[i].default_user = '';
          robots.robot[i].is_connected = false;
        }
      }
    }
    // ----------------------------

    // ---------------------------- origin: website ---------------------

    if (data.action === 'update_robots') {
      lookup[users[data.origin]].send(JSON.stringify({data: robots_list, action: 'update_robots'}))
    }

    if(data.action == 'set_pose' || data.action == 'set_joint' || data.action == 'play_motion'
      || data.action == 'disable_torque' || data.action == 'get_pose' || data.action == 'enable_mirror'
      || data.action == 'move_wheels' || data.action == 'stop_wheels' || data.action == 'set_expression'
      || data.action == 'play_scenario' || data.action == 'play_speech' || data.action == 'sleep'
      || data.action == 'move_wheels_scenario' || data.action == 'download_scenario' || data.action == 'download_motion'
      || data.action == 'download_speech' || data.action == 'set_button' || data.action == 'play_scenario_interpreter') {

      if (data.destiny) {
        var i = robots.robot.findIndex((r) => {return r.name === data.destiny});
        if(data.origin === robots.robot[i].default_origin) {
            lookup[robots_ids[data.destiny]].send(JSON.stringify(message))
        }
      }

      if (avatar_id != null) {
        console.log(avatar_id)
        lookup[avatar_id].send(JSON.stringify(message), function(error) {
          console.log('erro', error)
        });
      }

    }

    // -----------------------------

    // --------------------------- origin: robot -------------------------

    if(data.action == 'joints_read'){
      lookup[users[data.destiny]].send(JSON.stringify(message))
    }

    // ----------------------------


    last_message = data;
  });
  ws.send(JSON.stringify({'data': '', 'action': 'find_users', 'destiny': '', 'origin': 'server'}));
});

wss.broadcast = function broadcast(msg) {
  console.log('Broadcast: ', msg);
  wss.clients.forEach(function each(client) {
      client.send(msg);
   });
};

setInterval(() => {
	wss.broadcast(JSON.stringify({'data': '', 'action': 'ping', 'destiny': '', 'origin': 'server'}));
}, 10000);
